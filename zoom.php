<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- For Client View -->
    <!-- <link type="text/css" rel="stylesheet" href="https://source.zoom.us/2.3.5/css/bootstrap.css" /> -->
    <link rel="stylesheet" href="bootstrap.css">
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/2.3.5/css/react-select.css" />
</head>
<body>
    <p>Zoom</p>
    <div id="meetingSDKElement">
    <!-- Zoom Meeting SDK Rendered Here -->
    </div>
    <!-- For Component and Client View -->
    <script src="https://source.zoom.us/2.3.5/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/2.3.5/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/2.3.5/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/2.3.5/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/2.3.5/lib/vendor/lodash.min.js"></script>

    <!-- For Client View -->
    <script src="https://source.zoom.us/zoom-meeting-2.3.5.min.js"></script>

    <!-- For Component View -->
    <script src="https://source.zoom.us/2.3.5/zoom-meeting-embedded-2.3.5.min.js"></script>
    <script>

        ZoomMtg.preLoadWasm()
ZoomMtg.prepareWebSDK()
// loads language files, also passes any error messages to the ui
ZoomMtg.i18n.load('en-US')
ZoomMtg.i18n.reload('en-US')
ZoomMtg.setZoomJSLib('https://source.zoom.us/2.3.5/lib', '/av')

ZoomMtg.init({
  leaveUrl: 'http://localhost:9999/thanks-for-joining',
  success: (success) => {
    console.log(success)

    // ZoomMtg.join() will go here
  },
  error: (error) => {
    console.log(error)
  }
})

ZoomMtg.join({
  sdkKey: "gveoerLxQ6FTAieSES667ZqfUgpNZWGnUmSR",
  signature: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZGtLZXkiOiJndmVvZXJMeFE2RlRBaWVTRVM2NjdacWZVZ3BOWldHblVtU1IiLCJtbiI6Ijk5MjA3MTcwOTIiLCJyb2xlIjowLCJpYXQiOjE2NTIwMjM1MDYsImV4cCI6MTY1MjAzMDcwNiwiYXBwS2V5IjoiZ3Zlb2VyTHhRNkZUQWllU0VTNjY3WnFmVWdwTlpXR25VbVNSIiwidG9rZW5FeHAiOjE2NTIwMzA3MDZ9.4EhDsR22fzts8K0gK6g1zWIwDVPEJm7GCTcH9EvGvTg", // role in SDK Signature needs to be 0
  meetingNumber: "9920717092",
  passWord: "eFZpRlRsK01Sd2Mza3JSeGNoclMyUT09",
  userName: "Aung",
  success: (success) => {
    console.log(success)
  },
  error: (error) => {
    console.log(error)
  }
})


//         const client = ZoomMtgEmbedded.createClient()

//         let meetingSDKElement = document.getElementById('meetingSDKElement')

// client.init({
//   debug: true,
//   zoomAppRoot: meetingSDKElement,
//   language: 'en-US',
//   customize: {
//     meetingInfo: [
//       'topic',
//       'host',
//       'mn',
//       'pwd',
//       'telPwd',
//       'invite',
//       'participant',
//       'dc',
//       'enctype',
//     ],
//     toolbar: {
//       buttons: [
//         {
//           text: 'Custom Button',
//           className: 'CustomButton',
//           onClick: () => {
//             console.log('custom button')
//           }
//         }
//       ]
//     }
//   }
// })

// client.join({
//   sdkKey: "gveoerLxQ6FTAieSES667ZqfUgpNZWGnUmSR",
//   signature: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZGtLZXkiOiJndmVvZXJMeFE2RlRBaWVTRVM2NjdacWZVZ3BOWldHblVtU1IiLCJtbiI6Ijk5MjA3MTcwOTIiLCJyb2xlIjowLCJpYXQiOjE2NTIwMjM1MDYsImV4cCI6MTY1MjAzMDcwNiwiYXBwS2V5IjoiZ3Zlb2VyTHhRNkZUQWllU0VTNjY3WnFmVWdwTlpXR25VbVNSIiwidG9rZW5FeHAiOjE2NTIwMzA3MDZ9.4EhDsR22fzts8K0gK6g1zWIwDVPEJm7GCTcH9EvGvTg", // role in SDK Signature needs to be 0
//   meetingNumber: "9920717092",
//   password: "eFZpRlRsK01Sd2Mza3JSeGNoclMyUT09",
//   userName: "Aung"
// })
    //     ZoomMtg.join({
    // signature: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZGtLZXkiOiJndmVvZXJMeFE2RlRBaWVTRVM2NjdacWZVZ3BOWldHblVtU1IiLCJtbiI6Ijk0MTQwNjg0MjkxIiwicm9sZSI6MCwiaWF0IjoxNjUyMDIyMzU3LCJleHAiOjE2NTIwMjk1NTcsImFwcEtleSI6Imd2ZW9lckx4UTZGVEFpZVNFUzY2N1pxZlVncE5aV0duVW1TUiIsInRva2VuRXhwIjoxNjUyMDI5NTU3fQ.NIVUxQbq_U8xntUmnbgJ6BE-0dJnsr923yASoqKSNkE",
    // sdkKey: "gveoerLxQ6FTAieSES667ZqfUgpNZWGnUmSR",
    // userName: "Aung",
    // meetingNumber: "94140684291",
    // passWord: ""
    // })
    </script>
</body>
</html>